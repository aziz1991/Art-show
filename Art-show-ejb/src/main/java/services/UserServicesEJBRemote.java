package services;

import java.util.List;

import javax.ejb.Remote;

import entities.User;



@Remote
public interface UserServicesEJBRemote {
	public void addUser(User user);
	 public User findUserByID(int idUser);
	 public void deleteUser(User user);
	 public void updateUser(User user);
	 public List<User>findAllUser();
	 public List<User>findByUserName(String userName);
}
