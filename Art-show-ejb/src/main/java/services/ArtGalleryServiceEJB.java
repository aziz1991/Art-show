package services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import entities.ArtGallery;



/**
 * Session Bean implementation class ArtGalleryServiceEJB
 */
@Stateless
@LocalBean
public class ArtGalleryServiceEJB implements ArtGalleryServiceEJBRemote {
	@PersistenceContext
	EntityManager em;

    /**
     * Default constructor. 
     */
    public ArtGalleryServiceEJB() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void addArtGallery(ArtGallery ad) {
		em.persist(ad);
		
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteArtGallery(ArtGallery ad) {
		em.remove(em.merge(ad));
		
		
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateArtGallery(ArtGallery ad) {
		em.merge(ad);
		// TODO Auto-generated method stub
		
	}

	@Override
	public ArtGallery findArtGallery(int idArtGallery) {
		// TODO Auto-generated method stub
		return em.find(ArtGallery.class,idArtGallery );
	}

	@Override
	public List<ArtGallery> findAllArt() {
		return 		em.createQuery("select p from ArtGallery p",ArtGallery.class).getResultList();
		
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<ArtGallery> findArtByPrice(Float price) {
		// TODO Auto-generated method stub
		return em.createQuery("select a from ArtGallery a where a.price=?1",ArtGallery.class).setParameter(1,price).getResultList();

	}

}