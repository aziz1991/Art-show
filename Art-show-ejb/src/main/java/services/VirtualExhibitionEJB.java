package services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import entities.VirtualExhib;



/**
 * Session Bean implementation class ArtGalleryServiceEJB
 */
@Stateless
@LocalBean
public class VirtualExhibitionEJB implements VirtualExhibitionEJBRemote,VirtualExhibitionEJBLocal {
	@PersistenceContext
	EntityManager em;

    /**
     * Default constructor. 
     */
    public VirtualExhibitionEJB() {
        // TODO Auto-generated constructor stub
    }
    @Override
   	public void addExhibition(VirtualExhib exhibition) {
   		// TODO Auto-generated method stub
   		em.persist(exhibition);
   	}
    public void createExhibition(VirtualExhib exhibition){
    	em.persist(exhibition);
    }
	public void saveExhib(VirtualExhib exhibition){
		em.merge(exhibition);
	}
	public void removeExhibition(VirtualExhib exhibition){
		em.remove(exhibition);
	}
   
   	@Override
   	public VirtualExhib findExhibitionByID(int idGv) {
   		
   		return em.find(VirtualExhib.class, idGv);
   	}

   	@Override
   	public void deleteExhibition(VirtualExhib exhibition) {
   		em.remove(em.merge(exhibition));
   	}

   	@Override
   	public void updateExhibition(VirtualExhib exhibition) {
   		em.merge(exhibition);
   		
   	}
   	@Override
   	public VirtualExhib findExhibitionByType(String Type){
   		return em.createQuery("select e from VirtualExhib e where e.Type=:x",VirtualExhib.class)
   				.setParameter("x",Type).getSingleResult();
   	}
   	@Override
   	public List<VirtualExhib> findAllExhibition() 
   	 {
   		TypedQuery<VirtualExhib> T = em.createQuery("SELECT p FROM VirtualExhib p", VirtualExhib.class);
   		List<VirtualExhib> Virtual = T.getResultList();
   		return Virtual;	
   		}
	
	

}