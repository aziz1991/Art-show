package services;

import java.util.List;

import javax.ejb.Local;

import entities.VirtualExhib;

@Local
public interface VirtualExhibitionEJBLocal {
	void createExhibition(VirtualExhib exhibition);
	void saveExhib(VirtualExhib exhibition);
	VirtualExhib findExhibitionByID(int idGv);
	void removeExhibition(VirtualExhib exhibition);
	List<VirtualExhib> findAllExhibition() ;
	

}