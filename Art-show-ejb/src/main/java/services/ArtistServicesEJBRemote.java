package services;

import java.util.List;

import javax.ejb.Remote;

import entities.Artist;



@Remote
public interface ArtistServicesEJBRemote {
	public void addArtist(Artist artist);
	 public Artist findArtistByID(int idArtist);
	 public List<Artist>findArtistByUserName(String userName);
	 public void deleteArtist(Artist artist);
	 public void updateArtist(Artist artist);
	 public List<Artist>findAllArtist();
}
