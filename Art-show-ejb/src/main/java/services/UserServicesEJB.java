package services;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import entities.Artist;
import entities.User;


/**
 * Session Bean implementation class UserServicesEJB
 */
@Stateless
@LocalBean
public class UserServicesEJB implements UserServicesEJBRemote, UserServicesEJBLocal {
	@PersistenceContext
    EntityManager em;
    /**
     * Default constructor. 
     */
    public UserServicesEJB() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void addUser(User user) {
		em.persist(user);		
	}

	@Override
	public User findUserByID(int idUser) {
		return em.find(User.class , idUser);
	}

	@Override
	public void deleteUser(User user) {
		em.remove(em.merge(user));		
	}

	@Override
	public void updateUser(User user) {
		em.merge(user);
		
	}

	@Override
	public List<User> findAllUser() {
		
		return em.createQuery("select u from User u", User.class).getResultList();
	}

	@Override
	public List<User> findByUserName(String userName) {
		return em.createQuery("select a from User a where a.userName=?1",User.class).setParameter(1,userName).getResultList();
	}

	

	@Override
	public void saveCustomer(Artist artist) {
		// TODO Auto-generated method stub
		em.merge(artist);
	}

	
	

	public void createUser(User user) {
		em.persist(user);
	}

	

	public User authenticate(String login, String password) {
		User found = null;
		String jpql = "select u from User u where u.login=:login and u.password=:password";
		TypedQuery<User> query = em.createQuery(jpql, User.class);
		query.setParameter("login", login);
		query.setParameter("password", password);
		try {
			found = query.getSingleResult();
		} catch (Exception ex) {
			Logger.getLogger(UserServicesEJB.class.getName()).log(
					Level.WARNING,
					"authentication attempt failure with login=" + login
							+ " and password=" + password);
		}
		return found;
	}

	public List<User> findAllUsers() {
		return em.createQuery("select u from User u", User.class)
				.getResultList();
	}

	public boolean loginExists(String login) {
		boolean exists = false;
		String jpql = "select case when (count(u) > 0)  then true else false end from User u where u.login=:login";
		TypedQuery<Boolean> query = em.createQuery(jpql, Boolean.class);
		query.setParameter("login", login);
		try {
			exists = query.getSingleResult();
		} catch (NoResultException e) {
			Logger.getLogger(UserServicesEJB.class.getName()).log(Level.WARNING,
					"no user registred with login=" + login);
		}
		return exists;
	}

	public User findUserByLogin(String login) {
		User found = null;
		String jpql = "select u from User u where u.login=:login";
		TypedQuery<User> query = em.createQuery(jpql, User.class);
		query.setParameter("login", login);
		try {
			found = query.getSingleResult();
		} catch (Exception ex) {
			Logger.getLogger(UserServicesEJB.class.getName()).log(Level.WARNING,
					"no such login=" + login);
		}
		return found;
	}
}
