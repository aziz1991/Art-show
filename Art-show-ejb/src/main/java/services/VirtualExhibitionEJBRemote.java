package services;

import java.util.List;

import javax.ejb.Remote;

import entities.VirtualExhib;

@Remote
public interface VirtualExhibitionEJBRemote {
	void addExhibition(VirtualExhib exhibition);

	public VirtualExhib findExhibitionByID(int idGv);
	public void deleteExhibition(VirtualExhib exhibition);
	public void updateExhibition(VirtualExhib exhibition);
	public VirtualExhib findExhibitionByType(String Type);
	public List<VirtualExhib> findAllExhibition() ;


}