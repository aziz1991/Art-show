package services;

import java.util.List;

import javax.ejb.Remote;

import entities.ArtGallery;
import entities.VirtualExhib;



@Remote
public interface ArtGalleryServiceEJBRemote {
	public void  addArtGallery(ArtGallery ad);
	public void deleteArtGallery(ArtGallery ad);
	public void updateArtGallery(ArtGallery ad);
	public ArtGallery findArtGallery(int idArtGallery);
	 public List<ArtGallery> findAllArt();
	 public List<ArtGallery>findArtByPrice(Float price);


}