package services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import entities.Artist;



/**
 * Session Bean implementation class ArtistServicesEJB
 */
@Stateless
@LocalBean
public class ArtistServicesEJB implements ArtistServicesEJBRemote {
	@PersistenceContext
    EntityManager em;
    /**
     * Default constructor. 
     */
    public ArtistServicesEJB() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void addArtist(Artist artist) {
		em.persist(artist);
		
	}

	@Override
	public Artist findArtistByID(int idArtist) {
		return em.find(Artist.class , idArtist);
	}

	@Override
	public void deleteArtist(Artist artist) {
		em.remove(em.merge(artist));
		
	}

	@Override
	public void updateArtist(Artist artist) {
		em.merge(artist);
		
	}

	@Override
	public List<Artist> findAllArtist() {
		return em.createQuery("select a from Artist a", Artist.class).getResultList();
	}

	@Override
	public List<Artist>findArtistByUserName(String userName) {
		
		return em.createQuery("select a from Artist a where a.userName=?1",Artist.class).setParameter(1,userName).getResultList();
	}

}
