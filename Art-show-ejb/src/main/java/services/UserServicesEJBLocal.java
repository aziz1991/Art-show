package services;

import javax.ejb.Local;

import entities.Artist;
import entities.User;

import java.util.List;

@Local
public interface UserServicesEJBLocal {
	void createUser(User user);
	void saveCustomer(Artist artist);
	List<User> findAllUsers();
	User authenticate(String login, String password);
	boolean loginExists(String login);
	User findUserByLogin(String login);
}