package entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: UserArtGalleryId
 *
 */
@Embeddable

public class ArtistArtGalleryId implements Serializable {

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idArtGalleryPK;
		result = prime * result + idArtistPK;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ArtistArtGalleryId other = (ArtistArtGalleryId) obj;
		if (idArtGalleryPK != other.idArtGalleryPK)
			return false;
		if (idArtistPK != other.idArtistPK)
			return false;
		return true;
	}

	private int idArtistPK;
	private int idArtGalleryPK;
	private static final long serialVersionUID = 1L;

	public ArtistArtGalleryId() {
		super();
	}   
	 
	public int getIdArtistPK() {
		return idArtistPK;
	}
	public void setIdArtistPK(int idArtistPK) {
		this.idArtistPK = idArtistPK;
	}
	public int getIdArtGalleryPK() {
		return idArtGalleryPK;
	}
	public void setIdArtGalleryPK(int idArtGalleryPK) {
		this.idArtGalleryPK = idArtGalleryPK;
	}
	public int getIdArtGallery() {
		return this.idArtGalleryPK;
	}

	public void setIdCoursPK(int idArtGalleryPK) {
		this.idArtGalleryPK = idArtGalleryPK;
	}
   
}
