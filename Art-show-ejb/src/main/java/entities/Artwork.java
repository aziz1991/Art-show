package entities;

import java.io.Serializable;
import java.lang.Float;
import java.lang.String;
import javax.persistence.*;



/**
 * Entity implementation class for Entity: Artwork
 *
 */
@Entity

public class Artwork implements Serializable {

	   
	@Id
	private int idAw;
	private String type;
	private Float price;
	private String descrip;
	private int state;
	private String image;
	private String title;
	private String size;
	private String material;
	private int year;
	private static final long serialVersionUID = 1L;

	
	@ManyToOne
	private Artist artist;	
	 
	@ManyToOne
	private VirtualExhib vExhib;
	
	public Artwork() {
		super();
	}   
	public int getIdAw() {
		return this.idAw;
	}

	public void setIdAw(int idAw) {
		this.idAw = idAw;
	}   
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}   
	public Float getPrice() {
		return this.price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}   
	public String getDescrip() {
		return this.descrip;
	}

	public void setDescrip(String descrip) {
		this.descrip = descrip;
	}   
	public int getState() {
		return this.state;
	}

	public void setState(int state) {
		this.state = state;
	}   
	public String getImage() {
		return this.image;
	}

	public void setImage(String image) {
		this.image = image;
	}   
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}   
	public String getSize() {
		return this.size;
	}

	public void setSize(String size) {
		this.size = size;
	}   
	public String getMaterial() {
		return this.material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}   
	public int getYear() {
		return this.year;
	}

	public void setYear(int year) {
		this.year = year;
	}
   
}
