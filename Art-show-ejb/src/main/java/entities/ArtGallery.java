package entities;

import java.io.Serializable;
import java.lang.Float;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: ArtGallery
 *
 */
@Entity

public class ArtGallery implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idAg;
	private Float price;
	private String description;
	private String image;
	private String adresse;
	
	private static final long serialVersionUID = 1L;

	
	
	public ArtGallery() {
		super();
	}   
	public int getIdAg() {
		return this.idAg;
	}

	public void setIdAg(int idAg) {
		this.idAg = idAg;
	}   
	public Float getPrice() {
		return this.price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}   
	


	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
   
}
