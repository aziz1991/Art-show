package entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Reservation
 *
 */
@Entity

public class Reservation implements Serializable {

	   
	@Id
	private int idReservation;
	private Date dateReservation;
	private int durèe;
	private static final long serialVersionUID = 1L;
	
	@OneToOne 
	private VirtualExhib virtualexhib;
	@OneToOne 
	private Artist artist;

	public Reservation() {
		super();
	}   
	public int getIdReservation() {
		return this.idReservation;
	}

	public void setIdReservation(int idReservation) {
		this.idReservation = idReservation;
	}   
	public Date getDateReservation() {
		return this.dateReservation;
	}

	public void setDateReservation(Date dateReservation) {
		this.dateReservation = dateReservation;
	}   
	public int getDurèe() {
		return this.durèe;
	}

	public void setDurèe(int durèe) {
		this.durèe = durèe;
	}
	public VirtualExhib getVirtualexhib() {
		return virtualexhib;
	}
	public void setVirtualexhib(VirtualExhib virtualexhib) {
		this.virtualexhib = virtualexhib;
	}
	public Artist getArtist() {
		return artist;
	}
	public void setArtist(Artist artist) {
		this.artist = artist;
	}
   
}
