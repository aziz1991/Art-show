package entities;

import java.io.Serializable;
import java.lang.Float;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: userArtwork
 *
 */
@Entity

public class ClientArtwork implements Serializable {

	@EmbeddedId
	private ClientArtworkId clientArtworkId;
	private Float liveryCost;
	private Date liveryDate;
	private static final long serialVersionUID = 1L;

	

	@ManyToOne
	@JoinColumn(name="idClientPk",insertable=false,updatable=false)
	private  Client client;
	public ClientArtworkId getClientArtworkId() {
		return clientArtworkId;
	}

	public void setClientArtworkId(ClientArtworkId clientArtworkId) {
		this.clientArtworkId = clientArtworkId;
	}

	@ManyToOne
	@JoinColumn(name="idArtworkPk",insertable=false,updatable=false)
	private Artwork Artw;
	
	public ClientArtwork() {
		super();
	}   
	 
	public Float getLiveryCost() {
		return this.liveryCost;
	}

	public void setLiveryCost(Float liveryCost) {
		this.liveryCost = liveryCost;
	}   
	public Date getLiveryDate() {
		return this.liveryDate;
	}

	public void setLiveryDate(Date liveryDate) {
		this.liveryDate = liveryDate;
	}
   
}
