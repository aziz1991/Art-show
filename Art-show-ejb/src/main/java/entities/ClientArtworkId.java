package entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: UserArtworkId
 *
 */
@Embeddable

public class ClientArtworkId implements Serializable {

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idArtworkPK;
		result = prime * result + idClientPK;
		return result;
	}
	public int getIdClientPK() {
		return idClientPK;
	}
	public void setIdClientPK(int idClientPK) {
		this.idClientPK = idClientPK;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClientArtworkId other = (ClientArtworkId) obj;
		if (idArtworkPK != other.idArtworkPK)
			return false;
		if (idClientPK != other.idClientPK)
			return false;
		return true;
	}



	private int idClientPK;
	private int idArtworkPK;
	private static final long serialVersionUID = 1L;

	public ClientArtworkId() {
		super();
	}   
	  
	public int getIdArtworkPK() {
		return this.idArtworkPK;
	}

	public void setIdArtworkPK(int idArtworkPK) {
		this.idArtworkPK = idArtworkPK;
	}
   
}
