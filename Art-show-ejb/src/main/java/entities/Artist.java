package entities;

import entities.User;
import java.io.Serializable;

import java.lang.String;
import java.time.LocalDate;

import java.util.List;

import javax.persistence.*;


/**
 * Entity implementation class for Entity: Artist
 *
 */
@Entity

public class Artist extends User implements Serializable {

	   
	
	private String biography;
	private String image;
	private int isActive=1;
	private static final long serialVersionUID = 1L;

	@OneToMany(mappedBy="artist")
	private List <Artwork> listArtw;
	
	@OneToOne
	private Reservation reservation;
	
	
	public Artist() {
		super();
	}   
	   
	public Artist(String firstName, String secondName, String userName, String pwd, String email, int cin,
			LocalDate birthDay ,String adresse,String biography,String image) {
		super(firstName, secondName, userName, pwd, email, cin, birthDay,adresse);
		this.biography = biography;
		this.image = image;
	}

	public String getBiography() {
		return this.biography;
	}

	public void setBiography(String biography) {
		this.biography = biography;
	}   
	public String getImage() {
		return this.image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public int getIsActive() {
		return isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	
   
}
