package entities;


import java.io.Serializable;
import java.lang.String;
import java.time.LocalDate;



import javax.persistence.*;





/**
 * Entity implementation class for Entity: User
 *
 */
@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
public class User implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idUser;
	private String firstName;
	private String secondName;
	private String login;
	private String password;
	private String email;
	private int cin;
	private LocalDate birthDay;
	private String adresse;
	private static final long serialVersionUID = 1L;
	
	
	
	public User(String firstName, String secondName, String login, String password, String email, int cin,
			LocalDate birthDay,String adresse) {
		super();
		this.firstName = firstName;
		this.secondName = secondName;
		this.login = login;
		this.password = password;
		this.email = email;
		this.cin = cin;
		this.birthDay = birthDay;
		this.adresse = adresse;
	}

	

	public User() {
		super();
	}   
	public int getIdUser() {
		return this.idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}   
	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}   
	public String getSecondName() {
		return this.secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}   
	
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}   
	public int getCin() {
		return this.cin;
	}

	public void setCin(int cin) {
		this.cin = cin;
	}   
	public LocalDate getBirthDay() {
		return this.birthDay;
	}

	public void setBirthDay(LocalDate birthDay) {
		this.birthDay = birthDay;
	}



	public String getAdresse() {
		return adresse;
	}



	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}



	public String getLogin() {
		return login;
	}



	public void setLogin(String login) {
		this.login = login;
	}



	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}
	
	


	
}
