package entities;

import java.io.Serializable;
import java.lang.Float;
import java.lang.String;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: VirtualExhib
 *
 */
@Entity

public class VirtualExhib implements Serializable {

	   
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idVe;
	private String Name;
	private String descrip;
	private Float price;
	private String type;
	private String image;
	private Boolean dispo;
	private static final long serialVersionUID = 1L;
	@OneToOne
	private Reservation reservation;
	   
	public int getIdVe() {
		return this.idVe;
	}

	public void setIdVe(int idVe) {
		this.idVe = idVe;
	}   
	public String getDescrip() {
		return this.descrip;
	}

	public void setDescrip(String descrip) {
		this.descrip = descrip;
	}   
	public Float getPrice() {
		return this.price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}

	public Boolean getDispo() {
		return dispo;
	}

	public void setDispo(Boolean dispo) {
		this.dispo = dispo;
	}
	
}
