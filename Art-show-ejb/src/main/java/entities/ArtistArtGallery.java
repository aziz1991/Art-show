package entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;


/**
 * Entity implementation class for Entity: UserArtGallery
 *
 */
@Entity

public class ArtistArtGallery implements Serializable {

	   
	@EmbeddedId
	private  ArtistArtGalleryId artistArtGalleryId;
	private Date dateDebut;
	private Date dateFin;
	private static final long serialVersionUID = 1L;
	
	@ManyToOne
	@JoinColumn(name="idArtistPk",insertable=false,updatable=false)
	private  Artist artist;
	@ManyToOne
	@JoinColumn(name="idArtGalleryPk",insertable=false,updatable=false)
	private ArtGallery Artg;

	public ArtistArtGallery() {
		super();
	}   
	  
	

	public ArtistArtGalleryId getArtistArtGalleryId() {
		return artistArtGalleryId;
	}



	public void setArtistArtGalleryId(ArtistArtGalleryId artistArtGalleryId) {
		this.artistArtGalleryId = artistArtGalleryId;
	}



	public Date getDateDebut() {
		return this.dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}   
	public Date getDateFin() {
		return this.dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}
   
}
