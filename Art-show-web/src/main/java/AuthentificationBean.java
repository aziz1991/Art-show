
import javax.ejb.EJB;
//import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
//import javax.faces.bean.RequestScoped;
//import javax.faces.bean.SessionScoped;
//import javax.faces.bean.ViewScoped;
import javax.faces.bean.SessionScoped;

import entities.User;
import services.UserServicesEJBLocal;

@ManagedBean
//@ApplicationScoped 
@SessionScoped
//@ViewScoped
//@RequestScoped
public class AuthentificationBean {
	@EJB
	private UserServicesEJBLocal userServiceLocal;
	private User user = new User() ;
	


public String doLogin(){
	String navTo="";
	User found = userServiceLocal.authenticate(user.getLogin(),user.getPassword());
	if (found != null){
		user  = found;
		navTo="success?faces-redirect=true";
	}
	return navTo;
}
public User getUser() {
	return user;
}
public void setUser(User user) {
	this.user = user;
}
}
