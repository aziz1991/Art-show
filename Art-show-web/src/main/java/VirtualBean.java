
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import entities.VirtualExhib;
import services.VirtualExhibitionEJBLocal;


@ManagedBean
@ViewScoped
public class VirtualBean implements Serializable {

	private static final long serialVersionUID = -9107031652753540690L;

	@EJB
	private VirtualExhibitionEJBLocal catalog;


	private VirtualExhib virtualexhib;
	private List<VirtualExhib> virtualexhibs;
	private boolean formDisplayed;
	private List<VirtualExhib> filteredvirtualexhib;

	public VirtualBean() {
	}

	@PostConstruct
	public void init() {
		virtualexhib = new VirtualExhib();
		virtualexhibs = catalog.findAllExhibition();
	}

	public void doSave() {
		catalog.saveExhib(virtualexhib);
		virtualexhibs = catalog.findAllExhibition();
	}

	public void doNew() {
		virtualexhib = new VirtualExhib();
		formDisplayed = true;
	}

	public void doDelete() {
		catalog.removeExhibition(virtualexhib);
		virtualexhibs = catalog.findAllExhibition();
		formDisplayed = false;
	}

	public void doCancel() {
		formDisplayed = false;
	}
	
	public void onRowSelect() {
		formDisplayed = true;
	}
	
	public void onFilter(){
		virtualexhib = new VirtualExhib();
		formDisplayed = false;
	}

	

	

	public boolean isFormDisplayed() {
		return formDisplayed;
	}

	public void setFormDisplayed(boolean formDisplayed) {
		this.formDisplayed = formDisplayed;
	}

	

	public VirtualExhib getVirtualexhib() {
		return virtualexhib;
	}

	public void setVirtualexhib(VirtualExhib virtualexhib) {
		this.virtualexhib = virtualexhib;
	}

	public List<VirtualExhib> getVirtualexhibs() {
		return virtualexhibs;
	}

	public void setVirtualexhibs(List<VirtualExhib> virtualexhibs) {
		this.virtualexhibs = virtualexhibs;
	}

	public List<VirtualExhib> getFilteredvirtualexhib() {
		return filteredvirtualexhib;
	}

	public void setFilteredvirtualexhib(List<VirtualExhib> filteredvirtualexhib) {
		this.filteredvirtualexhib = filteredvirtualexhib;
	}

	

}
